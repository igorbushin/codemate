Usage:

    ruby check.rb [xib loc] [brief | short | full]

    xib - check outlets connectivity
    loc - check localization
    by default run all checkers

    brief - show only messages count
    short - show short messages text
    full - show full messages text
    by default show only messages count

Examples:

    $ ruby check.rb
    $ ruby check.rb xib
    $ ruby chekc.rb xib full
