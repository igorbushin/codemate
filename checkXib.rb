require_relative "./FilesManager.rb"
require_relative "./Logger.rb"
require_relative "./code.rb"

$outletXibMaskSimple = /<outlet (.*)\/>/
$outletXibMaskStrong = /<outlet property="(.*)" destination="(.*)" id="(.*)"\/>$/
#$actionXibMaskSimple = /<action (.*)\/>/
#$actionXibMaskStrong = /<action selector="(.*)" destination="(.*)" (.*)id="(.*)"\/>$/

def checkXib logger
  classes = buildClassesHierarchy
  xibPaths = ProjectManager.getXibPaths
  for xibPath in xibPaths
    xibName = File.basename xibPath
    xibStrings = (FilesManager.readFile xibPath).split "\n"
    #check outlets
    outletsXibCount = (extractStringsByMask xibStrings, $outletXibMaskSimple, 0).count
    outletsXibNames = extractStringsByMask xibStrings, $outletXibMaskStrong, 1
    if outletsXibCount != outletsXibNames.count
      logger.addFaultMessage "#{xibName}, can't parse #{outletsXibCount - outletsXibNames.count} outlets"
    end
    className = File.basename xibPath, ".xib"
    if classes[className] == nil
      logger.addFaultMessage "not found owner for #{xibName}"
    else
      properties = ["dataSource", "delegate"]
      while classes.has_key? className
        objcClass = classes[className]
        properties += objcClass.publicProperties
        properties += objcClass.privateProperties
        className = objcClass.parentClassName
      end
      brockenOutlets = outletsXibNames - properties
      logger.addMessage "brocken outlet(s)", "#{xibName}, not connected outlets: #{brockenOutlets}" unless brockenOutlets.empty?
    end
  end
  return "#{xibPaths.count} .xib files | dn't check delegate & dataSource"
end
