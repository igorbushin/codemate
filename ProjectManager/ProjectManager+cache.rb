require_relative "./ProjectManager+files.rb"
require_relative "./ProjectManager+localization.rb"

module ProjectManager
  class << self
    @@cache = {}
    methods = self.instance_methods false
    methods.each do |method|
      next unless instance_method(method).parameters.empty?
      alias_method "#{method}_uncached", "#{method}"
      define_method method do
        unless @@cache[method]
          #puts "call origin mehtod #{method}"
          methodResult = self.send "#{method}_uncached"
          @@cache[method] = methodResult
        end
        return @@cache[method]
      end
    end
  end
end

=begin
  projectFolder = ProjectManager.findProjectFolderOnPath File.dirname(__FILE__)
  puts "#{if projectFolder then projectFolder else "Fail!" end}"
  ProjectManager.getXibPaths
  ProjectManager.getXibPaths
  ProjectManager.getXibPaths
  ProjectManager.getXibPaths
=end
