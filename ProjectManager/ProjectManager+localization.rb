require_relative "./ProjectManager+files.rb"
require_relative "../CodeParser/CodeParser.rb"
require_relative "../Structures/LocalizationTable.rb"

module ProjectManager

  def self.getLocalizationTables
    localizationTables = []
    self.getFilePaths.each do |path|
      matchData = path.match /^(.*)\/(.*).lproj\/(.*).strings/
      next unless matchData
      localizationTable = LocalizationTable.new
      localizationTable.name = matchData[3]
      localizationTable.language = matchData[2]
      localizationTable.path = path
      tableContent = (FilesManager.readFile path).split "\n"
      localizationTable.localizations = CodeParser.parseLocalizationsFromTableContent tableContent
      localizationTables.push localizationTable
    end
    return localizationTables
  end

  def self.getExpectedLocalizations
    expectedLocalizations = {}
    self.getSourcePaths.each do |path|
      fileContent = FilesManager.readFile path
      localizations = CodeParser.searchLocalizationCalls fileContent
      expectedLocalizations.merge!(localizations) {|key, oldValue, newValue| (oldValue | newValue)}
    end
    return expectedLocalizations
  end

  def self.getLanguages
    localizationTables = self.getLocalizationTables
    languages = localizationTables.map { |table| table.language }
    return languages.uniq
  end

  def self.getLocalizationTablesNames
    localizationTables = self.getLocalizationTables
    localizationTablesNames = localizationTables.map { |table| table.name }
    return localizationTablesNames.uniq
  end
end

=begin
  projectFolder = ProjectManager.findProjectFolderOnPath File.dirname(__FILE__)
  puts "#{if projectFolder then projectFolder else "Fail!" end}"
  puts "#{ProjectManager.getLanguages}"
  puts "#{ProjectManager.getLocalizationTablesNames}"
  for table in ProjectManager.getLocalizationTables
    #puts table.localizations
  end
  puts ProjectManager.getExpectedLocalizations
=end
