require_relative "../FilesManager.rb"

module ProjectManager
  @@projectFolder = nil

  def self.findProjectFolderOnPath path
    path = File.absolute_path path
    while path != "/"
      subPaths = FilesManager.getPaths path, false
      subPaths.keep_if {|path| /.*\.xcodeproj/ =~ path}
      return @@projectFolder = path unless subPaths.empty?
      path = File.dirname path
    end
    return @@projectFolder =  nil
  end

  def self.projectFolder
    return @@projectFolder
  end

  def self.getPaths
    FilesManager.getPaths @@projectFolder, true
  end

  def self.getFilePaths
    return self.getPaths.select {|path| File.file? path}
  end

  def self.getXibPaths
    return self.getFilePaths.select {|path| /^(.*)\.xib$/ =~ path}
  end

  def self.getHeaderPaths
    return self.getFilePaths.select {|path| /^(.*)\.h$/ =~ path}
  end

  def self.getSourcePaths
    return self.getFilePaths.select {|path| /^(.*)\.m$/ =~ path}
  end

  def self.getCodePaths
    return self.getHeaderPaths + self.getSourcePaths
  end
end

=begin
  projectFolder = ProjectManager.findProjectFolderOnPath File.dirname(__FILE__)
  puts "#{if projectFolder then projectFolder else "Fail!" end}"
  #puts ProjectManager.getPaths
  #puts ProjectManager.getFilePaths
=end
