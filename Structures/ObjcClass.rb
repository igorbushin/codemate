class ObjcClass
  attr_accessor :parentClassName
  attr_accessor :publicProperties
  attr_accessor :privateProperties

  def initialize
    @publicProperties = []
    @privateProperties = []
  end

  def dbgPrint
    puts "parent: #{@parentClassName}"
    puts "public props: #{@publicProperties}"
    puts "private props: #{@privateProperties}"
  end
end
