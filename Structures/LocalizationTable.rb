class LocalizationTable
  attr_accessor :name
  attr_accessor :language
  attr_accessor :path
  attr_accessor :localizations

  def initialize
    @localizations = {}
  end
end
