require_relative "./ProjectManager/ProjectManager.rb"
require_relative "./checkLocalization.rb"
require_relative "./checkXib.rb"

begin
  if ARGV.include? "help"
    puts FilesManager.readFile "README.md"
  else
    currentFilePath = File.absolute_path __FILE__
    unless ProjectManager.findProjectFolderOnPath currentFilePath
      puts "not fount project folder"
      return
    end
    logLevels = ["brief", "short", "full"]
    logLevel = 0
    (logLevels & ARGV).each {|arg| logLevel = logLevels.index arg}
    checksDict = {"loc" => :checkLocalization,
                  "xib" => :checkXib}
    checksDict.select! {|key, check| ARGV.include? key} unless (ARGV & checksDict.keys).empty?
    for check in checksDict.values
      logger = Logger.new
      summary = self.send check, logger
      puts "#{check}: #{summary}"
      logger.putsMessages logLevel, "\t"
    end
  end
end
