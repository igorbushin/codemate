require_relative "./FilesManager.rb"
require_relative "./Structures/ObjcClass.rb"
require_relative "./strings.rb"

$importMask = /^#import "(.*)"/
$propertyMask = /^@property(.*)\*(\w*);/
$interfaceMask = /^@interface(\w*):(\w*)/
$categoryMask = /^@interface(\w*)\(.*\)/
$endMask = /^@end/

def buildClassesHierarchy
  classes = {}

  uiVC = ObjcClass.new
  uiVC.publicProperties.push "view"
  classes["UIViewController"] = uiVC

  uiTableCell = ObjcClass.new
  uiTableCell.publicProperties.push "view"
  classes["UITableViewCell"] = uiTableCell

  uiCollectionCell = ObjcClass.new
  uiCollectionCell.publicProperties.push "view"
  classes["UICollectionViewCell"] = uiCollectionCell

  for codePath in ProjectManager.getCodePaths
    code = (FilesManager.readFile codePath).split "\n"
    objcClass = nil
    for codeLine in code
      codeLine.delete! " \t\n"
      if (interface = codeLine.match $interfaceMask)
        className = interface[1]
        objcClass = classes[className]
        objcClass = ObjcClass.new unless objcClass
        objcClass.parentClassName = interface[2]
        classes[className] = objcClass
      elsif(category = codeLine.match $categoryMask)
        className = category[1]
        objcClass = classes[className]
        objcClass = ObjcClass.new unless objcClass
        classes[className] = objcClass
      elsif codeLine.match $endMask
        objcClass = nil
      elsif objcClass and codeLine.match $propertyMask
        propertyName = (codeLine.match $propertyMask)[2]
        if codePath[-1] == "h"
          objcClass.publicProperties.push propertyName
        else
          objcClass.privateProperties.push propertyName
        end
      end
    end
  end
  return classes
end
