require_relative "./Logger.rb"
require_relative "./ProjectManager/ProjectManager.rb"

def checkLocalization logger
  tablesByKeys = {}
  for localizationTable in ProjectManager.getLocalizationTables
    localizationTable.localizations.each do |key, translations|
      if translations.size > 1
        messageTitle = "key:#{key} table:#{localizationTable.name} lang:#{localizationTable.language}"
        if translations.uniq.size > 1
          logger.addMessage "key(s) has 2+ different translations in table", messageTitle
        else
          logger.addMessage "key(s) has 2+ identical translations in table", messageTitle
        end
      end
    end
  end
  ProjectManager.getExpectedLocalizations.each do |key, tables|

  end
  return "uncomplete checker"
end

=begin
$locMaskSimple = /NSLocalizedStringFromTable/
$locMaskStrong = /NSLocalizedStringFromTable\(@"(.*)",@"(.*)",nil\)/
$locMaskFuncSimple = /localizedFromTable:/
$locMaskFuncStrong = /@"(.*)"localizedFromTable:@"(.*)"/
  for keyValue in lexemes
    key = keyValue[0]
    lexema = keyValue[1]
    logger.addMessage "unused", key if lexema.tablesCalledFromCode.empty?
    for language in ProjectManager.getLanguages
      tablesForLanguage = lexema.tablesByLanguage[language]
      unless tablesForLanguage
        logger.addMessage "not localazed", "\"#{key}\" on #{language}"
        next
      end
      if (tablesForLanguage & lexema.tablesCalledFromCode).empty?
        missedTables = lexema.tablesCalledFromCode - tablesForLanguage
        logger.addMessage "localized in another table", "\"#{key}\" on #{language} #{missedTables}"
      end
      notUniqTables = tablesForLanguage.delete_if {|table| tablesForLanguage.count(table) == 1}
      unless notUniqTables.empty?
        logger.addMessage "multi localization", "\"#{key}\" on #{language} #{notUniqTables}"
      end
    end
  end
=end
