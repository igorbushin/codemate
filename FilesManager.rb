##
# Модуль для работы с файлами и директориями.
module FilesManager

  ##
  # Возвращает содержимое файла или nil если файл не найден.
  #
  # +filePath+ - путь к файлу.
  def self.readFile filePath
    return nil unless File.exist? filePath
    file = File.open filePath, "r"
    output = file.read#.toUTF8
    file.close
    return output
  end

  ##
  # Ищет файлы и поддиректории в +folder+ директории.
  #
  # Если флаг +recursive+ равен true то поиск происходит
  # во всем поддереве директории +folder+.
  def self.getPaths folder, recursive
    lsOutput = `ls \"#{folder}\"`.split "\n"
    paths = []
    for line in lsOutput
      path = "#{folder}/#{line}"
      if File.file? path
        paths.push path
      elsif File.directory? path
        paths.push path
        paths.concat self.getPaths path, true if recursive
      end
    end
    return paths
  end
end

=begin
  puts FilesManager.readFile __FILE__
  puts FilesManager.getPaths File.dirname(__FILE__), true
=end
