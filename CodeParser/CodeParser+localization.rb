require_relative "../FilesManager.rb"

module CodeParser
  def self.parseLocalizationsFromTableContent tableContent
    localizations = {}
    for line in tableContent
      matchData = line.match /^"(.*)" = "(.*)";$/
      next unless matchData
      key = matchData[1]
      localization = localizations[key]
      localization = [] unless localization
      localization.push matchData[2]
      localizations[key] = localization
    end
    return localizations
  end
end

=begin
  path = "/home/ingwine/iprofi-ios/iProfi/Localization/ru.lproj/EQLearn.strings"
  fileContent = (FilesManager.readFile path).split "\n"
  puts CodeParser.parseLocalizationsFromTableContent fileContent
  puts CodeParser.parseLocalizationsFromSourceFileContent "a"
=end
