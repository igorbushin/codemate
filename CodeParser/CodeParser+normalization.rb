module CodeParser
  def self.normalizeCode code
    normalizedCode = ""
    prevChar = nil
    meetBlanks = false
    blanks = " \n\t"
    alphanums = ("a".."z").to_a + ("A".."Z").to_a + ("0"..."9").to_a
    code.each_char do |char|
      if blanks.include? char
        meetBlanks = true
        next
      end
      if meetBlanks && alphanums.include?(char) && alphanums.include?(prevChar)
        normalizedCode += " "
      end
      meetBlanks = false
      normalizedCode += char
      prevChar = char
    end
    return normalizedCode
  end
end

=begin
  code =
"""bb @\"te\\\"xt\" b@\"x\"
NSString *my_string = @\"Line1 \\
  Line2\";
NSString *my_string = @\"Line1\"
                      \"Line2\";
NSString * query = @\"SELECT * FROM foo \"
                   @\"WHERE \"
"""
  normalizedCode = CodeParser.normalizeCode code
  puts "#{normalizedCode}"
=end
