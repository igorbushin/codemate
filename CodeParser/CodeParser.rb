require_relative "./CodeParser+commentsDeletion.rb"
require_relative "./CodeParser+normalization.rb"
require_relative "./CodeParser+stringsExtraction.rb"
require_relative "./CodeParser+localization.rb"

module CodeParser
  def self.prepareCodeForParsing code
    codeWithoutComments = self.removeCommentsFromCode code
    strings = self.extractStringsFromCode codeWithoutComments
    normalizedCode = self.normalizeCode codeWithoutComments
    code.replace normalizedCode
    return strings
  end

  def self.search code, mask
    results = []
    while code.rindex(mask) != nil
      rightIndex = code.rindex mask
      result = code[rightIndex..-1].match mask
      results.push result.to_a #if result
      code = code[0, rightIndex]
    end
    return results
  end

  #todo: move to category, speed up
  def self.searchLocalizationCalls code
    code = code.clone
    strings = self.prepareCodeForParsing code
    searchResult = self.search code, /NSLocalizedStringFromTable\(@"(.*)",@"(.*)",/
    localizationCalls = {}
    for match in searchResult
      key = strings[match[1].to_i]
      tableName = strings[match[2].to_i]
      localizationCalls[key] = [] unless localizationCalls[key]
      localizationCalls[key].push tableName unless localizationCalls[key].include? tableName
    end
    return localizationCalls
  end
end

=begin
  require_relative "../FilesManager.rb"
  path = "/home/ingwine/iprofi-ios/iProfi/Interface/ViewControllers/Tests/TestPassing/IPTestPassingVC.m"
  content = FilesManager.readFile path
  puts "#{(CodeParser.search content, /NSLocalizedStringFromTable/).size}"
  searchResult = CodeParser.searchLocalizationCalls content
  puts "#{searchResult.size} #{searchResult}"
=end
