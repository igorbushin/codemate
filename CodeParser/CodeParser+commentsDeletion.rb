module CodeParser

  ##
  # Метод возвращает текст из строки +code+, очищенный от комментариев.
  # В результате из текста вырезаются многострочные ( /\*...\*/ )
  # и однострочные ( //... ) комментарии.
  def self.removeCommentsFromCode code
    codeWithoutComments = ""
    isInsideOneLineComment = false
    isInsideMultiLineComment = false
    prevChar = nil
    code.each_char do |char|
      if isInsideOneLineComment && char == "\n"
        codeWithoutComments += "\n"
        isInsideOneLineComment = false
      elsif isInsideOneLineComment || isInsideMultiLineComment
        isInsideMultiLineComment = false if prevChar == "*" && char == "/"
      elsif prevChar == "/" && char == "/"
        isInsideOneLineComment = true
        codeWithoutComments.chop!
      elsif prevChar == "/" && char == "*"
        char = nil
        isInsideMultiLineComment = true
        codeWithoutComments.chop!
      elsif
        codeWithoutComments += char
      end
      prevChar = char
    end
    return codeWithoutComments
  end
end

=begin
  code = """a /* b
c
d */ e
f ///g /* h */
k /* l */ m /* n */ o // p
/*/ x*/ y"""
  res = CodeParser.removeCommentsFromCode code
  puts res
  #puts code
=end
