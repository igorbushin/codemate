module CodeParser

  ##
  # Метод возвращает массив строк ( "...", @"..." ), извлеченных из текста +code+.
  # Извлеченные строки в тексте заменяются на индекс в массиве.
  # Не поддерживается обьединение многострочных строк.
  def self.extractStringsFromCode code
    strings = []
    codeWithReplacedStrings = ""
    isInsideString = false
    prevChar = nil
    code.each_char do |char|
      if isInsideString && prevChar != "\\" && char == "\""
        isInsideString = false
        codeWithReplacedStrings += "\"#{strings.size - 1}\""
      elsif isInsideString
        strings[-1] += char
      elsif char == "\""
        isInsideString = true
        strings.push ""
      elsif
        codeWithReplacedStrings += char
      end
      prevChar = char
    end
    code.replace codeWithReplacedStrings
    return strings
  end
end

=begin
  code =
"""bb @\"te\\\"xt\" b@\"x\"
NSString *my_string = @\"Line1 \\
  Line2\";
NSString *my_string = @\"Line1\"
                      \"Line2\";
NSString * query = @\"SELECT * FROM foo \"
                   @\"WHERE \"
"""
  strings = CodeParser.extractStringsFromCode code
  puts "#{strings}"
  puts "#{code}"
=end
