##
# Класс для хранения и отображения сообщений
class Logger

  def initialize
    @log = {}
    @log["fault(s)"] = []
  end

  ##
  # Метод для добавления сообщения
  #
  # +type+ - тип сообщения, представленный строкой
  #
  # +title+ - краткое описание сообщения
  #
  # +description+ - подробное описание сообщения,
  # представленное массивом строк
  def addMessage type, title, description = []
    @log[type] = [] if @log[type] == nil
    @log[type].push [title, description]
  end

  ##
  # Метод для добавления сообщения о внутренней ошибке
  #
  # +title+ - краткое описание сообщения
  #
  # +description+ - подробное описание сообщения,
  # представленное массивом строк
  def addFaultMessage title, description = []
    self.addMessage "fault(s)", title, description
  end

  ##
  # Метод для отображения добавленных сообщений
  #
  # +level+ - уровень, отображающий подробность описания сообщений.
  # * 0 - отображается только количество сообщений.
  # * 1 - краткое описание.
  # * 2 - подробное описание.
  #
  # +types+ - массив типов сообщений, которые будут показаны.
  #
  # +prefix+ - префикс, добавляемый к каждой отображаемой строке
  def putsMessages level = 0, prefix = ""
    @log.each do |type, messages|
      puts "#{prefix}#{messages.count} #{type}"
      messages.each do |message|
        puts "#{prefix}\t#{message[0]}" if level >= 1
        puts message[1].map {|line| "#{prefix}\t\t#{line}"} if level >= 2
      end
    end
  end
end
