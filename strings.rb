def toUTF8 str
  str = str.force_encoding("UTF-8")
  return str if str.valid_encoding?
  str.encode "UTF-8", "binary", invalid: :replace, undef: :replace, replace: ""
end

def truncateString string, lenght
  if string.size > lenght
    return string[0, lenght - 3] + "..."
  else
    return string.clone
  end
end

def simplifyString string
  for i in 0...string.size
    string[i] = "*" unless string[i].match /[[:alpha:]]/
  end
  return string
end

def makeStringWithPattern patternAsString, key, value
  patternClone = patternAsString.clone
  patternClone[key] = value
  return patternClone
end

def extractStringsByMask strings, mask, index
  results = []
  for string in strings
    while string.rindex(mask) != nil
      rightIndex = string.rindex mask
      result = string[rightIndex..-1].match mask
      results.push result[index] if result
      string = string[0, rightIndex]
    end
  end
  return results
end

def scanStringsByMask strings, mask1, mask2
  results = []
  for string in strings
    while string.rindex(mask1) != nil
      rightIndex = string.rindex mask1
      result = string[rightIndex..-1].scan mask2
      results.concat result
      string = string[0, rightIndex]
    end
  end
  return results
end
